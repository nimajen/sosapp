package com.nimajen

import android.app.Application
import com.nimajen.sosapp.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class SosApplication  : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@SosApplication)
            androidLogger()
            modules(appModule)
        }
    }

}
package com.nimajen.sosapp

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.nimajen.sosapp.manager.MessageManager
import com.nimajen.sosapp.ui.alert.AlertFragment
import com.nimajen.sosapp.ui.configuration.ConfigurationFragment


class MainActivity : AppCompatActivity(), AlertFragment.AlertFragmentListener {

    companion object {
        private const val REQUEST_CODE_PERMISSION = 121
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, AlertFragment.newInstance())
                .commitNow()
        }
        if (!MessageManager.checkPermission(this)) {
            askPermission(this)
        }

        val notificationManager =
            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (!notificationManager.isNotificationPolicyAccessGranted) {
            val dialog =
                AlertDialog.Builder(this)
                    .setTitle(R.string.dialog_do_not_disturb_title)
                    .setMessage(R.string.dialog_do_not_disturb_content)
                    .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }
                    .setPositiveButton(android.R.string.ok) { dialog, _ ->
                        dialog.cancel()
                        val intent = Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS)
                        this.startActivity(intent)
                    }
                    .create()
            dialog.show()
        }
    }

    private fun askPermission(activity: Activity) {
        if (MessageManager.checkPermission(this)) {
            return
        }
        ActivityCompat.requestPermissions(
            activity,
            arrayOf(Manifest.permission.SEND_SMS),
            REQUEST_CODE_PERMISSION
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_PERMISSION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(
                        this,
                        "SMS is a mandatory permission to use the app, please allow it",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun onPreferenceClicked() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ConfigurationFragment.newInstance())
            .addToBackStack("ConfigurationFragment")
            .commit()
    }
}
package com.nimajen.sosapp.ui.configuration

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nimajen.sosapp.repository.ConfigurationRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class ConfigurationViewModel(private val configurationRepository: ConfigurationRepository) :
    ViewModel() {

    private val _messageLiveData = MutableLiveData<String>()
    val messageLiveData: LiveData<String> = _messageLiveData

    private val _nameLiveData = MutableLiveData<String>()
    val nameLiveData: LiveData<String> = _nameLiveData

    private val _phoneLiveData = MutableLiveData<String>()
    val phoneLiveData: LiveData<String> = _phoneLiveData

    private val _contactsLiveData = MutableLiveData<LinkedHashMap<String, String>>()
    val contactsLiveData: LiveData<out Map<String, String>> = _contactsLiveData

    private val compositeDisposable = CompositeDisposable()

    init {
        compositeDisposable.add(configurationRepository.getMessage()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { message -> _messageLiveData.value = message }
        )
        compositeDisposable.add(
            configurationRepository.getContactMap()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { map ->
                    _contactsLiveData.value = map
                })
    }

    fun onMessageChanged(name: String) {
        if (_messageLiveData.value != name) {
            _messageLiveData.value = name
            configurationRepository.saveMessage(name)
        }
    }

    fun onNameChanged(name: String) {
        if (_nameLiveData.value != name) {
            _nameLiveData.value = name
        }
    }

    fun onPhoneChanged(name: String) {
        if (_phoneLiveData.value != name) {
            _phoneLiveData.value = name
        }
    }

    fun onAddContactClicked() {
        val phoneValue = _phoneLiveData.value
        val nameValue = _nameLiveData.value
        if (phoneValue.isNullOrEmpty() || nameValue.isNullOrEmpty()) {
            return
        }
        compositeDisposable.add(
            configurationRepository.addContact(nameValue, phoneValue)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { newMap -> _contactsLiveData.value = newMap }
        )
    }

    fun onRemoveContactClicked(name: String) {
        compositeDisposable.add(
            configurationRepository.removeContact(name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { map ->
                    _contactsLiveData.value = map
                })
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}

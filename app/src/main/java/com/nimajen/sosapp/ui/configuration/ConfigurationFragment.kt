package com.nimajen.sosapp.ui.configuration

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.nimajen.sosapp.R
import com.nimajen.sosapp.ui.configuration.ConfigurationAdapter.ConfigurationAdapterListener
import kotlinx.android.synthetic.main.configuration_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class ConfigurationFragment : Fragment() {

    companion object {
        fun newInstance() = ConfigurationFragment()
    }

    private val viewModel: ConfigurationViewModel by viewModel()

    private val configurationAdapter = ConfigurationAdapter(object : ConfigurationAdapterListener {
        override fun onDeleteClicked(name: String) {
            viewModel.onRemoveContactClicked(name)
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.configuration_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.messageLiveData.observe(
            viewLifecycleOwner,
            Observer { newText ->
                if (inputMessage.text.toString() != newText) inputMessage.setText(newText)
            })

        inputMessage.addTextChangedListener(lambda = { input ->
            viewModel.onMessageChanged(input)
        })

        viewModel.nameLiveData.observe(
            viewLifecycleOwner,
            Observer
            { newName ->
                if (inputName.text.toString() != newName) inputName.setText(newName)
            })
        inputName.addTextChangedListener(lambda = { input ->
            viewModel.onNameChanged(input)
        })

        viewModel.phoneLiveData.observe(
            viewLifecycleOwner,
            Observer
            { newPhone ->
                if (inputPhone.text.toString() != newPhone) inputPhone.setText(newPhone)
            })
        inputPhone.addTextChangedListener(lambda = { input -> viewModel.onPhoneChanged(input) })

        addContact.setOnClickListener { viewModel.onAddContactClicked() }
        initContacts()
    }

    private fun initContacts() {
        viewModel.contactsLiveData.observe(
            viewLifecycleOwner,
            Observer { contactList ->
                configurationAdapter.setList(
                    contactList.toList().map { Contact(it.first, it.second) })
            }
        )
        contactList.adapter = configurationAdapter
        contactList.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun EditText.addTextChangedListener(lambda: (input: String) -> Unit) {
        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                lambda.invoke(s?.toString() ?: "")
            }
        })
    }
}

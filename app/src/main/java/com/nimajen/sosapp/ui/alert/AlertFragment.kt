package com.nimajen.sosapp.ui.alert

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.nimajen.sosapp.R
import kotlinx.android.synthetic.main.alert_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class AlertFragment : Fragment() {

    private var listener: AlertFragmentListener? = null

    companion object {
        fun newInstance() = AlertFragment()
    }

    private val viewModel: AlertViewModel by viewModel()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as AlertFragmentListener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.alert_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        alertButton.setOnClickListener { viewModel.onAlertClicked() }

        viewModel.alarmPlayingLiveData.observe(viewLifecycleOwner, Observer { isActive ->
            alertButton.setBackgroundColor(
                if (isActive) requireContext().getColor(android.R.color.holo_green_dark)
                else requireContext().getColor(android.R.color.holo_red_dark)
            )
        })
        viewModel.smsStateLiveData.observe(viewLifecycleOwner, Observer { state ->
            smsState.visibility = View.VISIBLE
            @Suppress("WHEN_ENUM_CAN_BE_NULL_IN_JAVA")
            smsState.text = when (state) {
                AlertViewModel.SendMessageState.SENDING -> getString(R.string.sms_sending)
                AlertViewModel.SendMessageState.SENT -> getString(R.string.sms_sent)
                AlertViewModel.SendMessageState.ERROR -> getString(R.string.sms_error)
            }
        })

        preferenceButton.setOnClickListener { listener?.onPreferenceClicked() }
    }

    interface AlertFragmentListener {

        fun onPreferenceClicked()

    }
}

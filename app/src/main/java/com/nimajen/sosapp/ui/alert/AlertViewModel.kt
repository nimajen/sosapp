package com.nimajen.sosapp.ui.alert

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nimajen.sosapp.manager.MessageManager
import com.nimajen.sosapp.manager.SoundManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class AlertViewModel(
    private val soundManager: SoundManager,
    private val messageManager: MessageManager
) : ViewModel() {

    private val _alarmPlayingLiveData = MutableLiveData<Boolean>(false)
    val alarmPlayingLiveData: LiveData<Boolean> = _alarmPlayingLiveData

    private val _smsStateLiveData = MutableLiveData<SendMessageState>()
    val smsStateLiveData: LiveData<SendMessageState> = _smsStateLiveData

    private val disposables = CompositeDisposable()

    fun onAlertClicked() {
        if (_alarmPlayingLiveData.value == false) {
            _alarmPlayingLiveData.value = true
            soundManager.startAlarm()
            _smsStateLiveData.value = SendMessageState.SENDING
            disposables.add(
                messageManager.sendMessage()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            if (it) {
                                _smsStateLiveData.value = SendMessageState.SENT
                            } else {
                                _smsStateLiveData.value = SendMessageState.ERROR
                            }
                        },
                        {
                            _smsStateLiveData.value = SendMessageState.ERROR
                        }
                    ))
        } else {
            _alarmPlayingLiveData.value = false
            soundManager.stopAlarm()
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    enum class SendMessageState {
        SENDING,
        SENT,
        ERROR
    }
}

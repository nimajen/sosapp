package com.nimajen.sosapp.ui.configuration

data class Contact(val name:String, val phoneNumber:String)
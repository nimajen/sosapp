package com.nimajen.sosapp.ui.configuration

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nimajen.sosapp.R

class ConfigurationAdapter(private val listener: ConfigurationAdapterListener) :
    RecyclerView.Adapter<ConfigurationAdapter.ContactViewHolder>() {

    private var data = ArrayList<Contact>()

    fun setList(list: List<Contact>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder =
        ContactViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.contact_cell, parent, false)
        )

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        with(holder) {
            val contact = data[position]
            name.text = contact.name
            phone.text = contact.phoneNumber
            delete.setOnClickListener { listener.onDeleteClicked(contact.name) }
        }
    }

    interface ConfigurationAdapterListener {
        fun onDeleteClicked(name: String)
    }

    inner class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.contactName)
        val phone: TextView = itemView.findViewById(R.id.contactPhone)
        val delete: View = itemView.findViewById(R.id.contactDelete)
    }
}
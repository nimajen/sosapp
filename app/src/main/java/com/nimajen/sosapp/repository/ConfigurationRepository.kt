package com.nimajen.sosapp.repository

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.nimajen.sosapp.R
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class ConfigurationRepository(private val context: Context) {

    companion object {
        private const val sharedPrefName = "MessageSharedPref"
        private const val messageKey = "MessageKey"
        private const val contactSharedPreferences = "ContactSharedPref"
    }

    private val contactSingle: Single<LinkedHashMap<String, String>>
    private var contactMap: LinkedHashMap<String, String>? = null

    private var message: String? = null
    private val messageSingle: Single<String>

    private var messagePreferences: SharedPreferences? = null
    private var contactPreferences: SharedPreferences? = null

    init {
        contactSingle = Single.fromCallable {
            val result = LinkedHashMap<String, String>()
            contactPreferences =
                context.getSharedPreferences(contactSharedPreferences, Context.MODE_PRIVATE)
            for (entry in contactPreferences!!.all) {
                result[entry.key] = entry.value as String
            }
            contactMap = result
            result
        }

        messageSingle = Single.fromCallable {
            messagePreferences = context.getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE)
            message = messagePreferences!!.getString(
                messageKey,
                context.getString(R.string.defaultMessage)
            )!!
            message
        }
    }

    fun getContactMap(): Single<LinkedHashMap<String, String>> {
        return if (contactMap != null) {
            Single.just(contactMap)
        } else {
            Single.fromObservable(contactSingle.toObservable())
                .subscribeOn(Schedulers.computation())
        }
    }

    fun getMessage(): Single<String> {
        return if (message != null) {
            Single.just(message)
        } else {
            Single.fromObservable(messageSingle.toObservable())
                .subscribeOn(Schedulers.computation())
        }
    }

    fun saveMessage(input: String) {
        //TODO refacto this code like addContact
        if (messagePreferences == null) {
            messagePreferences = context.getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE)
        }
        messagePreferences!!.edit().putString(messageKey, input).apply()
        message = input
    }

    fun addContact(name: String, phone: String): Single<LinkedHashMap<String, String>> {
        if (contactMap == null) {
            return Single.fromObservable(contactSingle.toObservable())
                .subscribeOn(Schedulers.computation())
                .map { map ->
                    addToContact(map, name, phone)
                    contactMap
                }
        } else {
            return Single.fromCallable {
                addToContact(contactMap!!, name, phone)
                contactMap
            }
        }
    }

    fun removeContact(name: String): Single<LinkedHashMap<String, String>> {
        if (contactMap == null) {
            return Single.fromObservable(contactSingle.toObservable())
                .subscribeOn(Schedulers.computation())
                .map { map ->
                    removeContact(map, name)
                    map
                }
        } else {
            return Single.fromCallable {
                removeContact(contactMap!!, name)
            }.subscribeOn(Schedulers.computation()).map { contactMap!! }
        }
    }

    @SuppressLint("ApplySharedPref")
    private fun removeContact(
        contacts: LinkedHashMap<String, String>,
        name: String
    ) {
        contacts.remove(name)
        contactPreferences!!.edit().remove(name).commit()
    }

    @SuppressLint("ApplySharedPref")
    private fun addToContact(contacts: LinkedHashMap<String, String>, name: String, phone: String) {
        contacts[name] = phone
        contactPreferences!!.edit().putString(name, phone).commit()
    }
}
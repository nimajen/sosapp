package com.nimajen.sosapp.di

import com.nimajen.sosapp.manager.MessageManager
import com.nimajen.sosapp.manager.SoundManager
import com.nimajen.sosapp.repository.ConfigurationRepository
import com.nimajen.sosapp.ui.alert.AlertViewModel
import com.nimajen.sosapp.ui.configuration.ConfigurationViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { SoundManager(get()) }

    factory { MessageManager(get(), get()) }

    single { ConfigurationRepository(get()) }

    viewModel { AlertViewModel(get(), get()) }

    viewModel { ConfigurationViewModel(get()) }

}
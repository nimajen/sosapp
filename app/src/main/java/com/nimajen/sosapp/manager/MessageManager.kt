package com.nimajen.sosapp.manager

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.telephony.SmsManager
import androidx.core.content.ContextCompat
import com.nimajen.sosapp.repository.ConfigurationRepository
import io.reactivex.Single
import io.reactivex.functions.BiFunction


class MessageManager(
    private val context: Context,
    private val configurationRepository: ConfigurationRepository
) {

    companion object {

        fun checkPermission(context: Context): Boolean =
            ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) ==
                    PackageManager.PERMISSION_GRANTED

    }

    fun sendMessage(): Single<Boolean> =
        configurationRepository.getContactMap().zipWith(
            configurationRepository.getMessage(),
            BiFunction<LinkedHashMap<String, String>, String, Boolean> { contacts, message ->
                if (!checkPermission(context)) {
                    throw IllegalStateException("Error with permission, allow sms first")
                }
                if (contacts.isEmpty()) {
                    false
                } else {
                    for (contact in contacts) {
                        sendMessageToContact(contact.value, message)
                    }
                    true
                }
            })

    private fun sendMessageToContact(contact: String, message: String) {
        val sms = SmsManager.getDefault()
        val textMessages = sms.divideMessage(message)
        sms.sendMultipartTextMessage(contact, null, textMessages, null, null)
    }
}



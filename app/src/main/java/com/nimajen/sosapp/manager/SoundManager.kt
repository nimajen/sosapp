package com.nimajen.sosapp.manager

import android.app.NotificationManager
import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log


class SoundManager(private val context: Context) {

    private var mediaPlayer: MediaPlayer? = null

    fun startAlarm() {
        disableDoNotDisturb()
        setSoundToMaximum()
        mediaPlayer?.stop()
        createMediaPlayer()
        mediaPlayer?.start()
    }

    private fun createMediaPlayer() {
        mediaPlayer = MediaPlayer().apply {
            try {
                setAudioAttributes(
                    AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_ALARM).build()
                )
                val uri: Uri =
                    Uri.parse("android.resource://" + context.packageName + "/raw/siren")
                setDataSource(context, uri)
                prepare()
                isLooping = true
            } catch (e: Exception) {
                Log.e("SoundManager", "Error with media prepare player", e)
            }
        }
    }

    fun stopAlarm() {
        mediaPlayer?.stop()
    }

    private fun setSoundToMaximum() {
        val audioService = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        audioService.setStreamVolume(
            AudioManager.STREAM_ALARM, audioService.getStreamMaxVolume(AudioManager.STREAM_ALARM), 0
        )
    }

    private fun disableDoNotDisturb() {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (notificationManager.isNotificationPolicyAccessGranted) {
            notificationManager.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_ALL)
        }
    }
}